package com.synofs.websockets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocketApplication {

	public static void main(String[] args) {
		System.out.println("Testing");
		SpringApplication.run(WebSocketApplication.class, args);
	}

}
